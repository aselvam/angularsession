demoapp.controller('RegisterController',['$scope', function($scope) {
	$scope.isRegistered=false
		$scope.register = function(user){
	 		console.log(user.name)
	 		console.log(user.email)
	 		console.log(user.mobile)
	 		$scope.isRegistered = true
	 	}
	 
}])

demoapp.controller('CartController',['$scope', function($scope) {
	$scope.invoice = {
                items: [{
                    qty: 10,
                    description: 'gadget',
                    cost: 9.95}]
            }
            
            $scope.addItem = function() {
                $scope.invoice.items.push({
                    qty: 1,
                    description: '',
                    cost: 0
                });
            }

            $scope.removeItem = function(index) {
                $scope.invoice.items.splice(index, 1);
            }

            $scope.total = function() {
                var total = 0;
                angular.forEach($scope.invoice.items, function(item) {
                    total += item.qty * item.cost;
                })

                return total;
            } 
}])


demoapp.controller('MiscController',['$scope', function($scope) {
	$scope.value1 = true;
    $scope.value2 = 'YES'
    $scope.text1 ="sample"
    $scope.word = /^\w*$/;
    $scope.list = [];
	$scope.text = 'hello';
	$scope.submit = function() {
		if (this.text) {
			this.list.push(this.text)
			this.text = ''
		}
	 }

	  $scope.items = ['settings', 'home', 'other']
	  $scope.selection = $scope.items[0]
   
}])


demoapp.controller('ProductController',['$scope',function($scope) {
	// $http.get('data/movies.json').success(function(data){
	// 	$scope.movies = data
	// });color:"blue", brand:"Lee cooper", size:"34"

   var data =  [

	{"id":"1597", "name": "Jeans", "desc":"Denim jeans","price":"$50", "color":"blue", "brand":"Lee cooper", "size":"34"},
	{"id":"2345","name": "Tops", "desc":"V neck style top","price":"$30","color":"grey", "brand":"Capture", "size":"12"},
	{"id":"5678","name": "TShirt", "desc":"Classic polo tshirt","price":"$20","color":"blue", "brand":"Emerge", "size":"Medium"}
   ]

   $scope.products = data

	$scope.sortorder="name"
}])

demoapp.controller('ProductDetailController',['$scope','$http', '$routeParams', function($scope,$http, $routeParams) {
	$scope.productid = $routeParams.productid;
}])




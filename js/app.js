var demoapp =angular.module('productapp',['ngRoute']).
	config(['$routeProvider','$locationProvider', function($routeProvider,$locationProvider){
		$routeProvider.
			when('/samples', {templateUrl:'partials/samples.html',controller:''}).
			when('/form', {templateUrl:'partials/form.html',controller:'RegisterController'}).
			when('/cart', {templateUrl:'partials/cart.html',controller:'CartController'}).
			when('/misc', {templateUrl:'partials/misc.html',controller:'MiscController'}).
			when('/products', {templateUrl:'partials/products.html',controller:'ProductController'}).
			when('/product/:productid', {templateUrl:'partials/product-detail.html',controller:'ProductDetailController'}).
			otherwise({redirectTo:'/samples'});
			//$locationProvider.html5Mode(true);
}]);	